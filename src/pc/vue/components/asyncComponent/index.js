
import Loading from '../loading/index';

// eslint-disable-next-line import/prefer-default-export
export function asyncComponent(component, height, loadingText) {
  const Com = () => ({
    // 需要加载的组件 (应该是一个 `Promise` 对象)
    component: component(),
    // 异步组件加载时使用的组件
    loading: Vue.component(new Date().getTime(), {
      extends: Loading,
      data() {
        return { height, text: loadingText };
      },
    }),
    // 加载失败时使用的组件
    // 展示加载时组件的延时时间。默认值是 200 (毫秒)
    delay: 200,
    // 如果提供了超时时间且组件加载也超时了，
    // 则使用加载失败时使用的组件。默认值是：`Infinity`
    timeout: 3000,
  });
  return Com;
}

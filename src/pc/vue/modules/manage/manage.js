/*
 * @Descripttion: 描述
 * @version: V1.0.0
 * @Author: Shuangshuang Song
 * @LastEditors: Shuangshuang Song
 * @Date: 2020-05-23 12:34:52
 * @LastEditTime: 2020-06-15 10:56:49
 */
import { asyncComponent } from '../../components/asyncComponent/index';

const Company = asyncComponent(() => import(/* webpackChunkName: "manage/company" */'./components/company/company'));
const OrgInfo = asyncComponent(() => import(/* webpackChunkName: "manage/orgInfo" */ './components/orgInfo/orgInfo'));
const Activity = asyncComponent(() => import(/* webpackChunkName: "manage/activity" */ './components/activity/activity'));
const LoginRecord = asyncComponent(() => import(/* webpackChunkName: "manage/login_record" */ './components/login_record/login_record'));
const Quick = asyncComponent(() => import(/* webpackChunkName: "manage/quick" */ './components/quick/quick'));

export default {
  name: 'manage',
  data() {
    return {};
  },
  computed: {},
  components: {
    Company,
    OrgInfo,
    Activity,
    LoginRecord,
    Quick,
  },
  mounted() { },
  methods: {
    load() {
      this.$refs.company.getCompanyInfo();
      this.$refs.orgInfo.getUserData();
      this.$refs.activity.getData();
      this.$refs.loginRecord.$refs.list.load();
      this.$refs.loginRecord.getData();
    },
  },
  watch: {},
  filters: {},
};

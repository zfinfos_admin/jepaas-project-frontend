const m = {
  createtransaction: 'Create',// 创建事务
  keyword: 'Keyword',// 关键字
  search: 'Search',// 查询
  delay: 'Delay',// 已延期
  publisher: 'Publisher',// 发布人
  finishjob: 'Finish the job',// 完成任务
  picktask: 'Pick up task',// 领取任务
  annotation: 'Annotation',// 批注
  boot: 'Boot',// 启动
  pause: 'Pause',// 暂停
  unstar: 'Unstar',// 取消加星
  star: 'Star',// 加星
  edit: 'Edit',// 编辑
  del: 'Delete',// 删除
}
export default m;
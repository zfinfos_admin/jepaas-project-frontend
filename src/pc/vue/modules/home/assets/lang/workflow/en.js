const m = {
  keyword: 'Keyword',
  date: 'Date',
  begin: 'Begin',
  end: 'End',
  search: 'Search',
  screening: 'Screening',
  unfinished: 'Unfinished',
  finished: 'Finished',
  canceldelay: 'Cancel delay',
  adddelay: 'Add delay',
  unmark: 'Unmark',
  mark: 'Mark',
  classification: 'Process classification',
}
export default m;
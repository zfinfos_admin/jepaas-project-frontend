import fetch from '../../../util/fetch';
import {
  POST_LOGIN,
  POST_SEND_VALIDATECODE,
  POST_UPDATE_PWD,
  GET_SEARCH_COMPANY,
  POST_JOIN_COMPANY,
  GET_INDUSTRY,
  POST_HAS_USER,
  POST_CHECK_VALIDATECODE,
  POST_REGISTER_ACOOUNT,
  POST_USER_CODE,
  POST_APP_CREATE_STATE,
  POST_APP_REGISTER_USER,
  POST_APP_DINGTALK_INFO,
  GET_DINGTALK_TOKEN,
  GET_DINGTALK_PERSISTENT_CODE,
  GET_DINGTALK_SNS_TOKEN,
  GET_DINGTALK_USERINFO,
  POST_SET_PHONE_QRCODE,
  POST_CHECK_USER,
} from './api';
// 简单加密参数
const compileStr = (params) => {
  const strs = { j_dept: 'd', j_username: 'u', j_password: 'p' };
  for (const key in strs) {
    if (params.hasOwnProperty(key)) {
      params[strs[key]] = window.btoa(window.btoa(params[key]));
      delete params[key];
    }
  }
};

/**
 * 登录
 * @param param
 * @returns {Promise<T | never>}
 */
export function fetchLogin(param) {
  // const { clientid, token } = JE.getPushInfo();
  compileStr(param);
  const data = Object.assign(param, {
    // apkId: JE.getApkID(),
    // apkName: JE.getLSItem('apkName'),
    // apkKey: JE.getLSItem('apkCode'),
    cid: 'phone',
    // token,
    // type: plus.os.name,
  });
  return fetch(POST_LOGIN, {}, {
    type: 'post',
    data,
  })
    .then(data => data)
    .catch();
}
/**
 * 检查用户
 * @param param
 * @returns {Promise<T | never>}
 */
export function checkingUser(param) {
  return fetch(POST_CHECK_USER, {}, {
    type: 'post',
    data: param,
  })
    .then(data => data)
    .catch();
}

/**
 *发送短信验证码
 * @param param
 * @returns {Promise<T | never>}
 */
export function fetchSendValidateCode(param) {
  return fetch(POST_SEND_VALIDATECODE, {}, {
    type: 'post',
    data: param,
  })
    .then(data => data)
    .catch();
}


/**
 *修改密码
 * @param param
 * @returns {Promise<T | never>}
 */
export function fetchUpdatePwd(param) {
  return fetch(POST_UPDATE_PWD, {}, {
    type: 'post',
    data: param,
  })
    .then(data => data)
    .catch();
}


/**
 *查询用户是否存在
 * @param param
 * @returns {Promise<T | never>}
 */
export function fetchHasUser(param) {
  return fetch(POST_HAS_USER, {}, {
    type: 'post',
    data: param,
  })
    .then(data => data)
    .catch();
}


/**
 *查询企业
 * @param param
 * @returns {Promise<T | never>}
 */
export function fetchSearchUserCompany(param) {
  return fetch(GET_SEARCH_COMPANY, param, {})
    .then(data => data)
    .catch();
}


/**
 *查询行业
 * @param param
 * @returns {Promise<T | never>}
 */
export function fetchGetIndustry(param) {
  return fetch(GET_INDUSTRY, {}, {
    type: 'post',
    data: param,
  })
    .then(data => data)
    .catch();
}

/**
 *添加公司
 * @param param
 * @returns {Promise<T | never>}
 */
export function fetchJoinCompany(param) {
  return fetch(POST_JOIN_COMPANY, {}, {
    type: 'post',
    data: param,
  })
    .then(data => data)
    .catch();
}

/**
 *核对验证码
 * @param param
 * @returns {Promise<T | never>}
 */
export function fetchCheckValidate(param) {
  return fetch(POST_CHECK_VALIDATECODE, {}, {
    type: 'post',
    data: param,
  })
    .then(data => data)
    .catch();
}

/**
 *注册并且绑定账号
 * @param param
 * @returns {Promise<T | never>}
 */
export function fetchRegsiterAccount(param) {
  return fetch(POST_REGISTER_ACOOUNT, {}, {
    type: 'post',
    data: param,
  })
    .then(data => data)
    .catch();
}

/**
 *查询用户绑定
 * @param param
 * @returns {Promise<T | never>}
 */
export function fetchSearchUserCode(param) {
  return fetch(POST_USER_CODE, {}, {
    type: 'post',
    data: param,
  })
    .then(data => data)
    .catch();
}
// export function fetchSearchUserCode(param) {
//   return fetch(POST_USER_CODE, {}, {
//     type: 'post',
//     data: param,
//   })
//     .then(data => data)
//     .catch();
// }

/**
 * 发送状态机
 * @param param
 * @returns {Promise<T | never>}
 */
export function fetchCreateState(param) {
  return fetch(POST_APP_CREATE_STATE, {}, {
    type: 'post',
    data: param,
  })
    .then(data => data)
    .catch();
}

/**
 * app 创建user
 * @param param
 * @returns {Promise<T | never>}
 */
export function fetchAppCreateUser(param) {
  return fetch(POST_APP_REGISTER_USER, {}, {
    type: 'post',
    data: param,
  })
    .then(data => data)
    .catch();
}

/**
 * 钉钉信息
 * @param param
 * @returns {Promise<T | never>}
 */
export function fetchAppDingTalkInfo(param) {
  return fetch(POST_APP_DINGTALK_INFO, {}, {
    type: 'post',
    data: param,
  })
    .then(data => data)
    .catch();
}


/**
 *获取钉钉token
 * @param param
 * @returns {Promise<T | never>}
 */
export function fetcDingTalkToken(param) {
  return fetch(GET_DINGTALK_TOKEN, param, {})
    .then(data => data)
    .catch();
}


/**
 * 获取钉钉持久授权码  url?access_token
 * @param param
 * @returns {Promise<T | never>}
 */
export function fetchDingTalkPersistent(getParam, param) {
  return fetch(GET_DINGTALK_PERSISTENT_CODE, getParam, {
    type: 'post',

    data: param,
  })
    .then(data => data)
    .catch();
}


/**
 * 获取钉钉SNS_TOKEN  url?access_token
 * @param param
 * @returns {Promise<T | never>}
 */
export function fetchDingTalkSNSToken(param) {
  return fetch(GET_DINGTALK_SNS_TOKEN, {}, {
    type: 'post',
    data: param,
  })
    .then(data => data)
    .catch();
}

/**
 * 获取钉钉用户信息  url?sns_token
 * @param param
 * @returns {Promise<T | never>}
 */
export function fetchDingTalkUserInfo(param) {
  return fetch(GET_DINGTALK_USERINFO, {}, {
    type: 'post',
    data: param,
  })
    .then(data => data)
    .catch();
}


/**
 *  确认登录扫二维码
 * @param param
 * @returns {Promise<T | never>}
 */
export function fetchPostPhoneQRCode(param) {
  return fetch(POST_SET_PHONE_QRCODE, {}, {
    type: 'post',
    data: param,
  })
    .then(data => data)
    .catch();
}

/*
 * @Descripttion:
 * @Author: qinyonglian
 * @Date: 2020-03-15 13:24:57
 * @LastEditors: qinyonglian
 * @LastEditTime: 2020-03-15 13:41:12
 */
import { POST_FIND_BUBBLE_MSG } from './api';
import fetch from '@/util/fetch';
/*
 *获取冒泡列表
 */
export function getBubbleList(param) {
  return fetch(POST_FIND_BUBBLE_MSG, null, {
    type: 'POST',
    data: param,
  })
    .then(data => data)
    .catch();
}
